#!/bin/bash
set -e
NAME=${1:-grav}
BACKUP_DIR=${2:-/backup}
DATE=$(date +%Y_%m_%d-%H_%M_%S)
TAR_FILE=backup-${NAME}-${DATE}.tgz
BACKUP_FILE=${BACKUP_DIR}/${TAR_FILE}

if [ -f ${BACKUP_FILE} ]; then
    echo "Existing backup file found at: [${BACKUP_FILE}]. Deleting..."
    rm -rf ${BACKUP_FILE}
    echo "Deleted ${BACKUP_FILE}"
fi

echo "Backing up to: ${BACKUP_FILE} ..."
tar cvzf ${BACKUP_FILE} /usr/share/nginx/html
echo "Done backing up."
