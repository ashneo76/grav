#!/bin/bash
set -e
TAR_FILE=${1:-}
BACKUP_DIR=${2:-/backup}

if [ ! -z ${NAME} ]; then
    BACKUP_FILE=${BACKUP_DIR}/${TAR_FILE}

    if [ ! -f ${BACKUP_FILE} ]; then
        echo "Restoring backup from: ${BACKUP_FILE}"
        tar xvzf ${BACKUP_FILE} -C /usr/share/nginx/html
        echo "Done restoring backup. Hopefully this works..."
    else
        echo "Cannot find backup file: ${BACKUP_FILE}"
    fi
else
    echo "No backup file specified. Skipping..."
fi
