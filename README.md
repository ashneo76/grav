# Grav Docker Image [![build status](https://gitlab.com/ashneo76/grav/badges/master/build.svg)](https://gitlab.com/ashneo76/grav/commits/master)

This is a simple prepackaged Grav image with Nginx and PHP-FPM installed.
