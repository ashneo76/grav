#!/bin/bash

set -euo pipefail

GRAV_PARENT=ashneo76/grav
HTML_ROOT_DIR=/usr/share/nginx/html

#### Parse Options
## Usage String
usage() {

cat << HELP_TEXT
usage: init_grav.sh [OPTIONS]

This script installs a barebones Grav container or a skeleton.

OPTIONS:
    -h      Show this message
    -n      Project name ( Required )
    -s      Server Name/CNAME [ Default: localhost ]
    -p      Project port [ Default: 80 ]
    -i      Install directory [ Default: \$(pwd) ]
    -t      Grav skeleton URL to use. Leave empty for barebones install. [ Default: (empty) ]
HELP_TEXT

}

## Set initial defaults
PROJ_NAME=
GRAV_PORT=
TGT_DIR=
GRAV_SKELETON_URL=
SERVER_NAME=

while getopts "hn:s:p:i:t:" OPTION
do
    case $OPTION in
        h)
            usage
            exit 1
            ;;
        n)
            PROJ_NAME=$OPTARG
            ;;
        s)
            SERVER_NAME=$OPTARG
            ;;
        p)
            GRAV_PORT=$OPTARG
            ;;
        i)
            TGT_DIR=$OPTARG
            ;;
        t)
            GRAV_SKELETON_URL=$OPTARG
            ;;
        ?)
            usage
            exit
            ;;
    esac
done

## Set defaults
if [[ -z ${PROJ_NAME} ]]; then
    PROJ_NAME=grav
fi

if [[ -z ${GRAV_PORT} ]]; then
    GRAV_PORT=80
fi

if [[ -z ${TGT_DIR} ]]; then
    TGT_DIR=$(pwd)
fi

if [[ -z ${SERVER_NAME} ]]; then
    SERVER_NAME=localhost
fi

echo "Project Name: ${PROJ_NAME}"
echo "Server Name: ${SERVER_NAME}"
echo "Server Port: ${GRAV_PORT}"
echo "Installation Directory: ${TGT_DIR}"
echo "Grav Skeleton URL: ${GRAV_SKELETON_URL}"
## we don't care if GRAV_SKELETON_URL is empty or not
####

GRAV_CTR_NAME=grav-${PROJ_NAME}
GRAV_CTR_DATA_NAME=${PROJ_NAME}-data
SCRIPTS_DIR=${TGT_DIR}/${PROJ_NAME}
PROJ_DIR=${SCRIPTS_DIR}/tmp-data
DOCKER_DIR=${SCRIPTS_DIR}/docker
SYSTEMD_FILE=${GRAV_CTR_NAME}.service

# Download Grav source code if necessary
if [ ! -d "${PROJ_DIR}" ]; then
    echo "Downloading grav..."
    git clone https://github.com/getgrav/grav.git ${PROJ_DIR}
fi

pushd ${PROJ_DIR}

# Initialize Git
if [ ! -d ".git" ]; then
    echo "Initializing git..."
    # git rm -rf .git
    git init
    git add .
    # git commit -m "Initial commit"
fi

# Setup / upgrade docker container
echo "Fetching latest docker image..."
docker pull ${GRAV_PARENT}

# Setting up the Dockerfile
echo "Creating the dockerfile"
mkdir -p ${DOCKER_DIR}
echo "FROM ${GRAV_PARENT}" > ${DOCKER_DIR}/Dockerfile

echo "Building the docker container..."
docker build -t ${GRAV_CTR_NAME} --build-arg SERVER_NAME=${SERVER_NAME} ${DOCKER_DIR}

echo "Setting up docker container..."
docker stop ${GRAV_CTR_NAME} 2>/dev/null || echo "Skip stopping ${GRAV_CTR_NAME}."
docker rm ${GRAV_CTR_NAME} 2>/dev/null || echo "Skip removing ${GRAV_CTR_NAME}."
docker rm -v ${GRAV_CTR_DATA_NAME} 2>/dev/null || echo "Skip removing ${GRAV_CTR_DATA_NAME}."
docker create -v ${HTML_ROOT_DIR} --name ${GRAV_CTR_DATA_NAME} ${GRAV_CTR_NAME} /bin/true
docker run --rm -p ${GRAV_PORT}:80 --name ${GRAV_CTR_NAME} --volumes-from ${GRAV_CTR_DATA_NAME} ${GRAV_CTR_NAME} /scripts/install_grav.sh

popd

# Generate service management scripts
echo "Generating management scripts in: ${SCRIPTS_DIR} ..."
if [ ! -d "${SCRIPTS_DIR}" ]; then
    mkdir -p ${SCRIPTS_DIR}
fi

echo "Generating ${SYSTEMD_FILE} ..."
cat > ${SCRIPTS_DIR}"/"${SYSTEMD_FILE} << SERVICE
[Unit]
Description = Grav service for ${GRAV_CTR_NAME}
After = docker.service

[Service]
Restart = always
ExecStartPre = -/usr/bin/docker pull ${GRAV_CTR_NAME}
ExecStartPre = -/usr/bin/docker stop ${GRAV_CTR_NAME}
ExecStratPre = -/usr/bin/docker rm ${GRAV_CTR_NAME}
ExecStart = /usr/bin/docker run \\
    -p ${GRAV_PORT}:80 \\
    --name ${GRAV_CTR_NAME} \\
    --volumes-from ${GRAV_CTR_DATA_NAME} \\
    ${GRAV_CTR_NAME}
ExecStop = /usr/bin/docker stop ${GRAV_CTR_NAME}

[Install]
WantedBy = multi-user.target
SERVICE

echo "Generating start.sh ..."
cat > ${SCRIPTS_DIR}/start.sh << START_SCRIPT_TXT
#!/bin/bash
set -e
echo "Starting ${GRAV_CTR_NAME} ..."
CTR_ID=\$(docker ps -qa --filter=name=${GRAV_CTR_NAME})
if [ -z \${CTR_ID} ]; then
    docker run -d -p ${GRAV_PORT}:80 --restart always --name ${GRAV_CTR_NAME} --volumes-from ${GRAV_CTR_DATA_NAME} ${GRAV_CTR_NAME}
else
    docker start ${GRAV_CTR_NAME}
fi
echo "Started ${GRAV_CTR_NAME}."
START_SCRIPT_TXT
chmod +x ${SCRIPTS_DIR}/start.sh

echo "Generating stop.sh ..."
cat > ${SCRIPTS_DIR}/stop.sh << STOP_SCRIPT_TXT
#!/bin/bash
set -e
echo "Stopping ${GRAV_CTR_NAME} ..."
docker stop ${GRAV_CTR_NAME}
echo "Stopped ${GRAV_CTR_NAME}."
STOP_SCRIPT_TXT
chmod +x ${SCRIPTS_DIR}/stop.sh

echo "Generating restart.sh ..."
cat > ${SCRIPTS_DIR}/restart.sh << RESTART_SCRIPT_TXT
#!/bin/bash
set -e
${SCRIPTS_DIR}/stop.sh
${SCRIPTS_DIR}/start.sh
RESTART_SCRIPT_TXT
chmod +x ${SCRIPTS_DIR}/restart.sh

echo "Generating update.sh ..."
cat > ${SCRIPTS_DIR}/update.sh << UPDATE_SCRIPT_TXT
#!/bin/bash
set -e
echo "Updating image for Grav: [${GRAV_PARENT}] ..."
docker pull ${GRAV_PARENT}
GRAV_CTR_ID=\$(docker ps -qa --filter=name=${GRAV_CTR_NAME})
if [ ! -z \${GRAV_CTR_ID} ]; then
    echo "Flushing old container."
    docker stop \${GRAV_CTR_ID}
    docker rm \${GRAV_CTR_ID}
    docker build -t ${GRAV_CTR_NAME} --build-arg SERVER_NAME=${SERVER_NAME} ${DOCKER_DIR}
fi
echo "Updated grav image."
UPDATE_SCRIPT_TXT
chmod +x ${SCRIPTS_DIR}/update.sh

echo "Generating backup.sh ..."
cat > ${SCRIPTS_DIR}/backup.sh << BACKUP_SCRIPT_TXT
#!/bin/bash
set -e
BACKUP_DIR=\${1:-\$(pwd)/backup}
mkdir -p \${BACKUP_DIR}
docker run --rm -v \${BACKUP_DIR}:/backup --volumes-from=${GRAV_CTR_DATA_NAME} ${GRAV_CTR_NAME} /scripts/backup.sh ${GRAV_CTR_NAME}
BACKUP_SCRIPT_TXT
chmod +x ${SCRIPTS_DIR}/backup.sh

echo "Generating restore.sh ..."
cat > ${SCRIPTS_DIR}/restore.sh << RESTORE_SCRIPT_TXT
#!/bin/bash
set -e
BACKUP_FILE=\${1:-}
BACKUP_DIR=\${2:-$(pwd)/backup}
DATA_CTR_ID=\$(docker ps -qa --filter=name=${GRAV_CTR_DATA_NAME})

if [ -z \${DATA_CTR_ID} ]; then
    echo "Deploying a new container: ${GRAV_CTR_DATA_NAME} ..."
    tar xvzf \${BACKUP_FILE} ${PROJ_DIR}
    pushd ${PROJ_DIR}

    popd
else
    echo "${GRAV_CTR_DATA_NAME} is running as: \${DATA_CTR_ID}."
    echo "Restoring from \${BACKUP_FILE} ..."
    docker run --rm -v \${BACKUP_DIR}:/backup --volumes-from=${GRAV_CTR_DATA_NAME} ${GRAV_CTR_NAME} /scripts/restore.sh \${BACKUP_FILE}
fi
echo "Done."
RESTORE_SCRIPT_TXT
chmod +x ${SCRIPTS_DIR}/restore.sh

echo "Generating enter.sh ..."
cat > ${SCRIPTS_DIR}/enter.sh << ENTER_SCRIPT_TXT
#!/bin/bash
set -e
docker exec -it ${GRAV_CTR_NAME} su -s /bin/bash www-data
ENTER_SCRIPT_TXT
chmod +x ${SCRIPTS_DIR}/enter.sh

echo "Generating status.sh ..."
cat > ${SCRIPTS_DIR}/status.sh << STATUS_SCRIPT_TXT
#!/bin/bash
set -e
CTR_ID=\$(docker ps -qa --filter=name=${GRAV_CTR_NAME})

if [ -z \${CTR_ID} ]; then
    echo "${GRAV_CTR_NAME} is not running."
else
    echo "${GRAV_CTR_NAME} is running as: \${CTR_ID}."
fi

DATA_CTR_ID=\$(docker ps -qa --filter=name=${GRAV_CTR_DATA_NAME})
if [ -z \${DATA_CTR_ID} ]; then
    echo "${GRAV_CTR_DATA_NAME} is not running."
else
    echo "${GRAV_CTR_DATA_NAME} is running as: \${DATA_CTR_ID}."
fi
STATUS_SCRIPT_TXT
chmod +x ${SCRIPTS_DIR}/status.sh

echo "Done generating management scripts."

# Clean up
echo "Cleaning up..."
rm -rf ${PROJ_DIR}

# TODO: Define finally cleanup function
#       `trap finish EXIT`
echo "Done."
